core = 7.x
api = 2

projects[drupal][version] = 7.26
projects[drupal][type] = core

projects[bot][version] = 1.3
projects[bot][subdir] = contrib
; See http://drupal.org/node/1147340
projects[bot][patch][bot_tell][url] = http://drupal.org/files/issues/bot_tell-1147340-1.patch
; Strip whitespace and colons from the end of a karma candidate.
projects[bot][patch][] = patches/bot_karma_colon.patch
; Tell the channel the updated karma level.
projects[bot][patch][] = patches/bot_karma_notify.patch

projects[bot_highlight_all][version] = 1.0-alpha1
projects[bot_highlight_all][subdir] = contrib

projects[bot_leash][version] = 1.x-dev
projects[bot_leash][subdir] = contrib

projects[bot_assembla][type] = module
projects[bot_assembla][subdir] = contrib
projects[bot_assembla][download][type] = git
projects[bot_assembla][download][url] = http://git.drupal.org/sandbox/deviantintegral/1559786.git
projects[bot_assembla][download][branch] = 7.x-1.x

projects[bot_sed][version] = 1.x-dev
projects[bot_sed][subdir] = contrib

libraries[profiler][download][type] = file
libraries[profiler][download][url] = http://ftp.drupal.org/files/projects/profiler-7.x-2.x-dev.tar.gz
